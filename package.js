Package.describe({
  name: 'parhelium:ractive-sortable',
  summary: 'Decorator sortable.',
  version: '0.0.1',
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use([
        'parhelium:ractive@0.6.0'
  ], ['client']);
  api.imply('parhelium:ractive@0.6.0')
  api.addFiles('ractive-sortable');
});


